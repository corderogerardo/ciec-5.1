<?php
/**
* 
*/

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
	
	public function run(){
		\DB::table('users')->insert(array (
			'name'=>'Gerardo',
			'email'=>'hacker@gerardocordero.me',
			'password'=>\Hash::make('secret')
			));
	}
	
}